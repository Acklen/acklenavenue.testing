using System;
using FluentAssertions;

namespace AcklenAvenue.Testing
{
    public static class EquivalenceExtensions
    {
        public static bool IsEquivalentTo(this object original, object next)
        {
            try
            {
                original.Should().BeEquivalentTo(next);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
    }
}