﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace AcklenAvenue.Testing
{
    public static class TaskSpecificationExtensions
    {
        public static AwaitResult<T> AwaitAsync<T>(this Task<T> task)
        {
            try
            {
                task.Wait();
            }
            catch (AggregateException e)
            {
                if (e.InnerExceptions.Count == 1)
                {
                    throw e.InnerExceptions.First();
                }
                throw;
            }

            return new AwaitResult<T>(task);
        }

        public static AwaitResult AwaitAsync(this Task task)
        {
            try
            {
                task.Wait();
            }
            catch (AggregateException e)
            {
                if (e.InnerExceptions.Count == 1)
                {
                    throw e.InnerExceptions.First();
                }
                throw;
            }
            return new AwaitResult(task);
        }
    }
}