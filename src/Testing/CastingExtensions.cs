﻿using System;
using System.Threading.Tasks;

namespace AcklenAvenue.Testing
{
    public static class CastingExtensions
    {
        public static T CastAs<T>(this object obj)
        {
            try
            {
                return (T) obj;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("That object is of type '{0}' and cannot be cast as a '{1}'.",
                    obj.GetType().Name, typeof (T).Name));
            }

        }
    }
}