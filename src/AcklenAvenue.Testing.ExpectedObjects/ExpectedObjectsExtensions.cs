﻿using ExpectedObjects;
using FluentAssertions.Primitives;

namespace AcklenAvenue.Testing.ExpectedObjects
{
    public static class ExpectedObjectsExtensions
    {
        public static void BeLike(this ObjectAssertions assertions, object expected)
        {
            expected.ToExpectedObject().ShouldEqual(assertions.Subject);
        }

        public static void ShouldBeLikeExpected(this object actual, object expected)
        {
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        public static void ShouldBeSimilarToExpected(this object actual, object expected)
        {
            expected.ToExpectedObject().IgnoreTypes().ShouldEqual(actual);
        }

        public static void IsExpectedToBeLike(this object actual, object expected)
        {
            expected.ToExpectedObject().ShouldEqual(actual);
        }

        public static void IsExpectedToBeSimilar(this object actual, object expected)
        {
            expected.ToExpectedObject().IgnoreTypes().ShouldEqual(actual);
        }
    }
}