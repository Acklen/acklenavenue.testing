using System.Collections.Generic;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace AcklenAvenue.Testing.Moq.Specs.Expressions
{
    public class when_mocking_a_list_method_with_an_func_arg_that_is_not_a_match
    {
        static Mock<ITestRepository> _mock;
        static IEnumerable<TestPerson> _result;
        static string _messageLogged;
        static Mock<IExpressionLogger> _mockLogger;

        Establish context = () =>
        {
            _mockLogger = new Mock<IExpressionLogger>();

            _mock = new Mock<ITestRepository>();

            var sally = new TestPerson {Name = "sally"};

            var verifiedExpression = new ExpressionComparisonBuilder<TestPerson>(_mockLogger.Object)
                .ThatMatches(sally)
                .Build();

            _mock
                .Setup(x =>
                    x.Query(verifiedExpression))
                .Returns(new List<TestPerson>
                {
                    sally
                });
        };

        Because of = () => _result = _mock.Object.Query<TestPerson>(x => x.Name == "sam");

        It should_log_nothing = () => _mockLogger.VerifyNoOtherCalls();

        It should_return_an_empty_list = () => _result.Should().BeEmpty();
    }
}