using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace AcklenAvenue.Testing.Moq.Specs.Expressions
{
    public class when_mocking_a_method_with_an_expression_arg_that_is_not_a_match
    {
        static Mock<ITestRepository> _mock;
        static TestPerson _result;
        static Mock<IExpressionLogger> _mockLogger;

        Establish context = () =>
        {
            _mockLogger = new Mock<IExpressionLogger>();
            _mockLogger.Setup(x =>
                x.Log(
                    "The expression passed in from the production code did not match the required object:\r\nName=sally"));

            _mock = new Mock<ITestRepository>();

            var sally = new TestPerson {Name = "sally"};


            _mock
                .Setup(x => x.FindFirst(new ExpressionComparisonBuilder<TestPerson>(_mockLogger.Object)
                    .ThatMatches(sally)
                    .Build()
                ))
                .Returns(sally);
        };

        Because of = () => _result = _mock.Object.FindFirst<TestPerson>(x => x.Name == "sam");

        It should_log_the_non_matching_expression = () => { _mockLogger.Verify(); };

        It should_return_null = () => _result.Should().BeNull();
    }
}