using System.Collections.Generic;
using System.Linq;

namespace AcklenAvenue.Testing.EntityFramework
{
    public static class AsyncEnumerableExtensions{
        public static IQueryable<T> AsAsyncQueryable<T>(this IEnumerable<T> queryable)
        {
            return new TestAsyncEnumerable<T>(queryable);
        }
    }
}