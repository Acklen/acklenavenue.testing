using System.ComponentModel;

namespace AcklenAvenue.Testing.Moq
{
    public static class ObjectPrinter
    {
        public static string PrintToString(object obj)
        {
            var props = "";
            foreach(PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
            {
                var name=descriptor.Name;
                var value=descriptor.GetValue(obj);
                props += $"\r\n{name}={value}";
            }

            return props;
        }
    }
}