namespace AcklenAvenue.Testing.Moq
{
    public interface IExpressionLogger
    {
        void Log(string message);
    }
}