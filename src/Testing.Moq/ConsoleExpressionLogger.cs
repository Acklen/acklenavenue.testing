using System;

namespace AcklenAvenue.Testing.Moq
{
    public class ConsoleExpressionLogger : IExpressionLogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}