using System;
using System.Linq.Expressions;
using Moq;

namespace AcklenAvenue.Testing.Moq
{
    public class Some<T>
    {
        public static T With(Expression<Func<T, bool>> func)
        {
            return It.Is<T>(func);
        }
    
        public static T Like(T comparisonObject)
        {
            return It.Is<T>(x => x.IsEquivalentTo(comparisonObject));
        }
    }
}